from django.contrib import admin

from .models import EmailObject, ErrorLog

class EmailObjectAdmin(admin.ModelAdmin):
    list_display = ("to", "subject", "added", "is_sended", )
    list_filter = ("is_sended", )

admin.site.register(EmailObject, EmailObjectAdmin)

class ErrorLogAdmin(admin.ModelAdmin):
    list_display = ("message", "is_sended", )
    list_filter = ("is_sended", )

admin.site.register(ErrorLog, ErrorLogAdmin)
