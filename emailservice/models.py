from django.db import models
from django.core.mail import send_mail

class EmailObject(models.Model):
    to = models.CharField(max_length=150, verbose_name="User email")
    subject = models.CharField(max_length=150)
    text = models.CharField(max_length=2500)
    added = models.DateTimeField(auto_now_add=True)
    retries = models.IntegerField(default=0)
    is_sended = models.BooleanField(default=False)
    send_date = models.DateTimeField(null=True, blank=True)
    error = models.TextField(null=True, blank=True)
    priority = models.IntegerField(default=5)
    
    def __str__(self):
        return self.to



class ErrorLog(models.Model):
    # log errors so we can send them to admins, or browse
    message = models.TextField()
    is_sended = models.BooleanField(default=False)
    added = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.message
