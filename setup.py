import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="emailservice",
    version="0.0.1",
    author="webii",
    author_email="webii@pm.me",
    description="Django app for email sending and storing",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/pi11/django-email-service/",
    packages=setuptools.find_packages(),
    classifiers=[
                 "Programming Language :: Python :: 3",
                 "Framework :: Django"
                 "License :: OSI Approved :: MIT License",
                 "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
